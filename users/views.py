import logging

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from users.forms import UserForm, ContactForm
from users.models import Utilisateur, Projet

logger = logging.getLogger(__name__)


@login_required(login_url='/users/login')
def me(request):
    user = Utilisateur.objects.get(username=request.user.username)
    projects = Projet.objects.all().filter(creation__idUser=user.id)

    if request.method == "POST":
        user.username = request.POST['username']
        user.first_name = request.POST['first_name']
        user.last_name = request.POST['last_name']
        user.email = request.POST['email']
        if request.POST['password'] != "":
            user.password = request.POST['password']
            user.set_password(user.password)
            user.save()
            user = authenticate(request, username=user.username, password=user.password)
            if user is not None:
                if user.is_active:
                    login(request, user)
        else:
            user.save()
    return render(request, 'profile.html', {'projects': projects})


@login_required(login_url='/users/login')
def iduser(request, iduser):
    user = Utilisateur.objects.get(id=iduser)
    projects = Projet.objects.all().filter(creation__idUser=user.id)
    return render(request, 'public_profile.html', {'projects': projects, 'myuser': user})


def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'login.html', context)


def register(request):
    if request.user.is_authenticated:
        return render(request, 'index.html')

    form = UserForm(request.POST or None)
    if form.is_valid():
        if request.POST['captcha'].lower() != "extremely secured" and request.POST[
            'captcha'].lower() != "extremelysecured":
            return render(request, 'you_are_a_robot.html')

        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        email = form.cleaned_data['email']
        user = Utilisateur(username=username,
                           password=password,
                           first_name=first_name,
                           last_name=last_name,
                           email=email)
        user.set_password(password)
        user.save()
        user = authenticate(request, username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(request.GET.get('next', '/'))
    context = {
        "form": form,
    }
    return render(request, 'register.html', context)


def login_user(request):
    if request.user.is_authenticated:
        return render(request, 'index.html')

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(request.GET.get('next', '/'))
            else:
                return render(request, 'login.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'login.html', {'error_message': 'Invalid login or password'})
    return render(request, 'login.html')


def forbidden(request):
    return render(request, 'forbidden.html')


def not_found(request):
    return render(request, 'not_found.html')


def legal(request):
    return render(request, 'legal.html')


def contact(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        if request.POST['captcha'].lower() != "extremely secured" and request.POST[
            'captcha'].lower() != "extremelysecured":
            return render(request, 'you_are_a_robot.html')
        subject = form.cleaned_data['subject']
        message = form.cleaned_data['message']
        sender = form.cleaned_data['sender']
        cc_myself = form.cleaned_data['cc_myself']
        recipients = ['contact@clovearth.com']
        if cc_myself:
            recipients.append(sender)
        send_mail(subject, message, sender, recipients)
        return HttpResponseRedirect('projects/')
    context = {
        "form": form,
    }
    return render(request, 'contact.html', context)
