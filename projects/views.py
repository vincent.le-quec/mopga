import hashlib
import hmac
import logging

from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Q
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from users.models import Utilisateur, Projet, Financement, Creation, Evaluation

logger = logging.getLogger(__name__)

# payment service data
api_secret = '002001000000002_KEY1'
merchant_id = "002001000000002"

# number of evaluation needed to validate a project
nb_evaluation = 2
# minimum of karma needed for a project to be accepted
min_karma = 2
# karma needed to become an evaluator
evaluator_karma = 100


def porteur_required():
    return user_passes_test(lambda u: u.porteur, login_url='/users/forbidden')


def evaluateur_required():
    return user_passes_test(lambda u: u.evaluateur, login_url='/users/forbidden')


def financeur_required():
    return user_passes_test(lambda u: u.financeur, login_url='/users/forbidden')


def index(request):
    query = request.GET.get('search')
    if query is None:
        projects = Projet.objects.filter(Q(etat="Funding") | Q(etat="Closed"))
    else:
        projects = Projet.objects.filter(
            (Q(etat="Funding") | Q(etat="Closed")) &
            Q(titre__icontains=query)
        )
    return render(request, 'projects.html', {'projects': projects})


@login_required(login_url='/users/login')
def idproject(request, idproject):
    i = Projet.objects.filter(id=idproject).count()

    if i <= 0:
        return render(request, 'not_found.html')

    myuser = Utilisateur.objects.get(username=request.user.username)
    projet = Projet.objects.get(id=idproject)
    user = Creation.objects.get(idProject__id=projet.id).idUser

    # les non evaluateurs ne peuvent accéder aux projets non évalué
    if projet.etat != "Funding" and projet.etat != "Closed" and not myuser.evaluateur:
        return render(request, 'forbidden.html')

    progressNoLimit = projet.montant * 100 / projet.montantMax
    if projet.montant < projet.montantMax:
        progress = projet.montant * 100 / projet.montantMax
    else:
        progress = 100
    return render(request, 'project.html',
                  {'projet': projet, 'myuser': user, 'progress': progress, 'progress_no_limit': progressNoLimit})


@login_required(login_url='/users/login')
@porteur_required()
def new(request):
    if request.method == 'GET':
        return render(request, 'new_project.html')

    myuser = Utilisateur.objects.get(username=request.user.username)

    titre = request.POST.get("title", "")
    description = request.POST.get("description", "")
    montantMax = request.POST.get("montant_max", "10")

    project = Projet()
    project.titre = titre
    project.description = description
    project.montantMax = montantMax
    project.save()

    link = Creation()
    link.idUser = myuser
    link.idProject = project
    link.save()

    return render(request, 'modification_accepted.html', {
        'title': 'Ajout d\'un project',
        'p': 'L\'ajout a bien été pris en compte',
        'redirect_url': '/users/me',
        'redirect_text': 'Vos projets'
    })


@login_required(login_url='/users/login')
@evaluateur_required()
def evaluate(request):
    if request.method == 'GET':
        query = request.GET.get('search')
        if query is None:
            projects = Projet.objects.filter(etat="Created")
        else:
            projects = Projet.objects.filter(
                Q(etat="Created") &
                Q(titre__icontains=query)
            )
        return render(request, 'projects.html', {'projects': projects})

    project_id = request.POST.get("project_id", "")
    note = request.POST.get("note", "")

    myuser = Utilisateur.objects.get(username=request.user.username)
    project = Projet.objects.get(id=project_id)

    if Evaluation.objects.filter(idUser=myuser).filter(idProject=project).count() > 0:
        return render(request, 'modification_refused.html', {
            'title': 'Evaluation refusée',
            'h2': 'Evaluation impossible',
            'p': 'Vous avez déjà évalué ce projet. Impossible de l\'évaluer à nouveau',
            'redirect_url': '/projects/evaluate',
            'redirect_text': 'Liste de projets à évaluer'
        })

    if project.etat != "Created":
        return render(request, 'modification_refused.html', {
            'title': 'Evaluation refusée',
            'h2': 'Erreur lors de la notation',
            'p': 'Ce projet ne peut plus être noté',
            'redirect_url': '/projects/evaluate',
            'redirect_text': 'Liste de projets à évaluer'
        })

    evaluation = Evaluation()
    evaluation.note = note
    evaluation.idUser = myuser
    evaluation.idProject = project
    evaluation.save()

    evaluations = Evaluation.objects.filter(idProject=project_id)
    if evaluations.count() >= nb_evaluation:
        moy_karma = 0
        for e in evaluations:
            moy_karma += e.note
        project.karma = moy_karma / evaluations.count()
        if project.karma >= min_karma:
            project.etat = "Funding"
        else:
            project.etat = "Rejected"
        project.save()

    return render(request, 'modification_accepted.html', {
        'title': 'Evaluation d\'un projet',
        'p': 'Votre avis a bien été pris en compte',
        'redirect_url': '/projects/evaluate',
        'redirect_text': 'Vos projets'
    })


@login_required(login_url='/users/login')
@financeur_required()
def donate(request):
    amount = request.POST.get("amount", "10")
    project_id = request.POST.get("project_id", "")

    myuser = Utilisateur.objects.get(username=request.user.username)
    project = Projet.objects.get(id=project_id)

    if project.etat != "Funding":
        return render(request, 'modification_refused.html', {
            'title': 'Projet non validé',
            'h2': 'Ce projet est en attente de validation.',
            'p': 'Merci d\'attendre que les modérateurs valident ce projet avant de faire des dons',
            'redirect_url': '/projects/evaluate',
            'redirect_text': 'Liste de projets à évaluer'
        })

    # do not exceed max amount
    if project.montant + int(amount) > project.montantMax:
        amount = project.montantMax - project.montant

    # save transaction
    transaction = Financement()
    transaction.montant = amount
    transaction.karma_claimed = False
    transaction.state = "Created"
    transaction.idUser = myuser
    transaction.idProject = project
    transaction.save()

    # prepare payment payload
    # documentation du logiciel de paiement : https://documentation.sips.worldline.com/fr/WLSIPS.317-UG-Sips-Paypage-POST.html
    return_address = "http://" + request.get_host() + "/projects/payment?project_id=" + str(project_id) \
                     + "&transaction_id=" + str(transaction.id)
    data = "amount=" + amount + "00|currencyCode=978|merchantId=" + merchant_id + "|normalReturnUrl=" + return_address + "|sealAlgorithm=HMAC-SHA-256|keyVersion=1"
    seal = hmac.new(bytes(api_secret, 'latin-1'), msg=bytes(data, 'latin-1'), digestmod=hashlib.sha256).hexdigest()

    # redirect to payment engine
    return render(request, 'donate.html', {'data': data, 'seal': seal})


@csrf_exempt
def payment(request):
    # retrieve my data
    transaction_id = request.GET.get("transaction_id", "")
    project_id = request.GET.get("project_id", "")
    data = request.POST.get("Data", "")
    seal = request.POST.get("Seal", "")

    # verify seal
    # documentation du logiciel de paiement : https://documentation.sips.worldline.com/fr/WLSIPS.317-UG-Sips-Paypage-POST.html
    myseal = hmac.new(bytes(api_secret, 'latin-1'), msg=bytes(data, 'latin-1'), digestmod=hashlib.sha256).hexdigest()
    if seal != myseal:
        return render(request, 'modification_refused.html', {
            'title': 'Payment refused',
            'h2': 'An Error has occurred',
            'p': 'Your contribution to the project ' + str(project_id) + ' has NOT been taken into account',
            'redirect_url': '/projects/' + str(project_id),
            'redirect_text': 'Back to project'
        })

    # get response
    response_code = data.split("responseCode=")[1].split("|")[0]
    transaction_ref = data.split("transactionReference=")[1].split("|")[0]

    # save payment
    projet = Projet.objects.get(id=project_id)
    financement = Financement.objects.get(id=transaction_id)
    financement.transaction_ref = transaction_ref

    # protect from submitting payment multiple times
    if financement.state != "Created":
        return render(request, 'forbidden.html')

    if response_code == "00":
        # update project amount
        projet.montant += financement.montant
        projet.save()

        financement.state = "Accepted"
        financement.save()

        # distribute karma if goal achieved
        if projet.montant >= projet.montantMax:
            projet.etat = "Closed"
            projet.save()
            projectCreation = Creation.objects.get(idProject=projet.id)
            projectCreation.idUser.karma += projet.karma
            projectCreation.idUser.save()

            for f in Financement.objects.filter(idProject=projet).filter(state="Accepted").filter(karma_claimed=False):
                f.idUser.karma += projet.karma
                if f.idUser.karma >= evaluator_karma:
                    f.idUser.evaluateur = True
                f.karma_claimed = True
                f.idUser.save()
                f.save()

        return render(request, 'modification_accepted.html', {
            'title': 'Payment accepted',
            'p': 'Your contribution to the project ' + str(project_id) + ' has been taken into account',
            'redirect_url': '/projects/' + str(project_id),
            'redirect_text': 'Back to project'
        })
    else:
        financement.state = "Refused"
        financement.save()
        return render(request, 'modification_refused.html', {
            'title': 'Payment refused',
            'h2': 'An Error has occurred',
            'p': 'Your contribution to the project ' + str(project_id) + ' has NOT been taken into account',
            'redirect_url': '/projects/' + str(project_id),
            'redirect_text': 'Back to project'
        })


@login_required(login_url='/users/login')
def logo(request):
    return render(request, "logo.html")


@csrf_exempt
def uploadImage(request):
    from django.core.files.storage import default_storage
    from django.http import HttpResponse
    from mopga import settings

    file = request.FILES['image']
    file_name = default_storage.save(settings.MEDIA_ROOT + "/" + file.name, file)
    final_filename = file_name.split('/')[-1]
    return HttpResponse(settings.MEDIA_URL + final_filename)
