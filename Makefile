export IMAGE_NAME=mopga

build:
	docker-compose -p mopga-dev -f docker-compose.yml -f docker-compose.dev.yml up -d --build

run:
	docker-compose -p mopga-dev -f docker-compose.yml -f docker-compose.dev.yml up
	#docker exec -it mopga-dev_django_1 bash ./docker-entrypoint_dev.sh

#import:
#	mysql -u root -p'root' -h 127.0.0.1 -P 8001 mopga < import.sql

dump:
	mysqldump -u root -p'root' -h 127.0.0.1 -P 8001 mopga > import.sql
	chown $(USER) import.sql

prod:
	docker-compose -p mopga-prod -f docker-compose.yml -f docker-compose.prod.yml up --build

clean-dev:
	docker-compose -p mopga-dev down

clean-prod:
	docker-compose -p mopga-prod down

.PHONY: build run prod dump clean
