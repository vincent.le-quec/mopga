-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: mopga
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add content type',4,'add_contenttype'),(14,'Can change content type',4,'change_contenttype'),(15,'Can delete content type',4,'delete_contenttype'),(16,'Can view content type',4,'view_contenttype'),(17,'Can add session',5,'add_session'),(18,'Can change session',5,'change_session'),(19,'Can delete session',5,'delete_session'),(20,'Can view session',5,'view_session'),(21,'Can add utilisateur',6,'add_utilisateur'),(22,'Can change utilisateur',6,'change_utilisateur'),(23,'Can delete utilisateur',6,'delete_utilisateur'),(24,'Can view utilisateur',6,'view_utilisateur'),(25,'une personne qui peut porter un projet',6,'porteur'),(26,'une personne qui peut évaluer le karma d\'un projet',6,'evaluateur'),(27,'une personne qui peut financer un projet',6,'financeur'),(28,'Can add projet',7,'add_projet'),(29,'Can change projet',7,'change_projet'),(30,'Can delete projet',7,'delete_projet'),(31,'Can view projet',7,'view_projet'),(32,'Can add financement',8,'add_financement'),(33,'Can change financement',8,'change_financement'),(34,'Can delete financement',8,'delete_financement'),(35,'Can view financement',8,'view_financement'),(36,'Can add evaluation',9,'add_evaluation'),(37,'Can change evaluation',9,'change_evaluation'),(38,'Can delete evaluation',9,'delete_evaluation'),(39,'Can view evaluation',9,'view_evaluation'),(40,'Can add creation',10,'add_creation'),(41,'Can change creation',10,'change_creation'),(42,'Can delete creation',10,'delete_creation'),(43,'Can view creation',10,'view_creation');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_users_utilisateur_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_users_utilisateur_id` FOREIGN KEY (`user_id`) REFERENCES `users_utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'contenttypes','contenttype'),(5,'sessions','session'),(10,'users','creation'),(9,'users','evaluation'),(8,'users','financement'),(7,'users','projet'),(6,'users','utilisateur');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2021-01-14 13:18:11.185758'),(2,'contenttypes','0002_remove_content_type_name','2021-01-14 13:18:11.291517'),(3,'auth','0001_initial','2021-01-14 13:18:11.375279'),(4,'auth','0002_alter_permission_name_max_length','2021-01-14 13:18:11.644368'),(5,'auth','0003_alter_user_email_max_length','2021-01-14 13:18:11.657768'),(6,'auth','0004_alter_user_username_opts','2021-01-14 13:18:11.670908'),(7,'auth','0005_alter_user_last_login_null','2021-01-14 13:18:11.684294'),(8,'auth','0006_require_contenttypes_0002','2021-01-14 13:18:11.689949'),(9,'auth','0007_alter_validators_add_error_messages','2021-01-14 13:18:11.702651'),(10,'auth','0008_alter_user_username_max_length','2021-01-14 13:18:11.716774'),(11,'auth','0009_alter_user_last_name_max_length','2021-01-14 13:18:11.730078'),(12,'auth','0010_alter_group_name_max_length','2021-01-14 13:18:11.761772'),(13,'auth','0011_update_proxy_permissions','2021-01-14 13:18:11.788886'),(14,'users','0001_initial','2021-01-14 13:18:12.092845'),(15,'admin','0001_initial','2021-01-14 13:18:12.906650'),(16,'admin','0002_logentry_remove_auto_add','2021-01-14 13:18:13.050742'),(17,'admin','0003_logentry_add_action_flag_choices','2021-01-14 13:18:13.069167'),(18,'sessions','0001_initial','2021-01-14 13:18:13.099624'),(19,'users','0002_auto_20210114_1333','2021-01-14 13:35:32.304063'),(20,'users','0003_auto_20210114_1335','2021-01-14 13:35:32.326907'),(21,'users','0002_auto_20210114_1640','2021-01-14 16:40:19.575461'),(22,'users','0003_auto_20210115_1236','2021-01-15 12:36:17.600292'),(23,'users','0004_auto_20210115_1353','2021-01-15 13:53:42.456971');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('7p3jrknitothqw95d3sr1mixx3j91p6n','NjllZTJiMTBlZWUwZGVjYzY2YWJlMTUwNmRjM2NkNDYwOGIzMGE1Yjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4ZTMzMjM1MzZlODJlMjQ2NWM4NDcxN2ZmMjIxZmE4MTlmYTFmNThhIn0=','2021-01-28 15:03:40.202762'),('hkxukk2j89dwcz8g95evn02kn1ihdgov','NjllZTJiMTBlZWUwZGVjYzY2YWJlMTUwNmRjM2NkNDYwOGIzMGE1Yjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4ZTMzMjM1MzZlODJlMjQ2NWM4NDcxN2ZmMjIxZmE4MTlmYTFmNThhIn0=','2021-01-28 13:35:49.020080'),('hudpqopj3fmjgmkxhi6dcfcw01wmthy2','ZjFhNDlmMzVjNzIwOTdlYTA4NzBiYjQzZWQzYWUwNmYzYzc2Y2UxMTp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YjVlZmVlODQ5MDZkNjlmYzU5MDA5ZjFiMzM4NGUzYzBjMGE3ZWZiIn0=','2021-01-29 13:15:29.755856');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_creation`
--

DROP TABLE IF EXISTS `users_creation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_creation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idProject_id` int NOT NULL,
  `idUser_id` int NOT NULL,
  `date` datetime(6) NOT NULL,
  `karma_claimed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_creation_idProject_id_7edbb05c_fk_users_projet_id` (`idProject_id`),
  KEY `users_creation_idUser_id_08e9ab6a_fk_users_utilisateur_id` (`idUser_id`),
  CONSTRAINT `users_creation_idProject_id_7edbb05c_fk_users_projet_id` FOREIGN KEY (`idProject_id`) REFERENCES `users_projet` (`id`),
  CONSTRAINT `users_creation_idUser_id_08e9ab6a_fk_users_utilisateur_id` FOREIGN KEY (`idUser_id`) REFERENCES `users_utilisateur` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_creation`
--

LOCK TABLES `users_creation` WRITE;
/*!40000 ALTER TABLE `users_creation` DISABLE KEYS */;
INSERT INTO `users_creation` VALUES (1,3,1,'2021-01-14 16:40:19.225682',0),(2,4,1,'2021-01-14 16:40:19.225682',0),(3,5,1,'2021-01-14 16:40:19.225682',0),(4,6,1,'2021-01-14 16:40:19.225682',0),(5,7,1,'2021-01-14 16:58:30.527108',0),(6,8,7,'2021-01-15 13:18:42.257623',0),(7,1,6,'2021-01-15 13:18:42.257623',0),(8,2,6,'2021-01-15 13:18:42.257623',0);
/*!40000 ALTER TABLE `users_creation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_evaluation`
--

DROP TABLE IF EXISTS `users_evaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_evaluation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `note` int unsigned NOT NULL,
  `idProject_id` int NOT NULL,
  `idUser_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_evaluation_idProject_id_7fa29dd5_fk_users_projet_id` (`idProject_id`),
  KEY `users_evaluation_idUser_id_a8dffa75_fk_users_utilisateur_id` (`idUser_id`),
  CONSTRAINT `users_evaluation_idProject_id_7fa29dd5_fk_users_projet_id` FOREIGN KEY (`idProject_id`) REFERENCES `users_projet` (`id`),
  CONSTRAINT `users_evaluation_idUser_id_a8dffa75_fk_users_utilisateur_id` FOREIGN KEY (`idUser_id`) REFERENCES `users_utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_evaluation`
--

LOCK TABLES `users_evaluation` WRITE;
/*!40000 ALTER TABLE `users_evaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_financement`
--

DROP TABLE IF EXISTS `users_financement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_financement` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` datetime(6) NOT NULL,
  `karma_claimed` tinyint(1) NOT NULL,
  `state` varchar(20) NOT NULL,
  `transaction_ref` varchar(50) NOT NULL,
  `idProject_id` int NOT NULL,
  `idUser_id` int NOT NULL,
  `montant` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_financement_idProject_id_3ae853a9_fk_users_projet_id` (`idProject_id`),
  KEY `users_financement_idUser_id_b6f35f4c_fk_users_utilisateur_id` (`idUser_id`),
  CONSTRAINT `users_financement_idProject_id_3ae853a9_fk_users_projet_id` FOREIGN KEY (`idProject_id`) REFERENCES `users_projet` (`id`),
  CONSTRAINT `users_financement_idUser_id_b6f35f4c_fk_users_utilisateur_id` FOREIGN KEY (`idUser_id`) REFERENCES `users_utilisateur` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_financement`
--

LOCK TABLES `users_financement` WRITE;
/*!40000 ALTER TABLE `users_financement` DISABLE KEYS */;
INSERT INTO `users_financement` VALUES (1,'2021-01-14 15:03:48.561634',0,'Accepted','2021011416034816c3',3,1,10);
/*!40000 ALTER TABLE `users_financement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_projet`
--

DROP TABLE IF EXISTS `users_projet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_projet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titre` varchar(256) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `etat` varchar(10) NOT NULL,
  `montant` double NOT NULL,
  `montantMax` double NOT NULL,
  `karma` int unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_projet`
--

LOCK TABLES `users_projet` WRITE;
/*!40000 ALTER TABLE `users_projet` DISABLE KEYS */;
INSERT INTO `users_projet` VALUES (1,'Test','Lorem Venatum','Created',0,10,0),(2,'Test 2','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ultrices, massa at venenatis gravida, nibh ante dictum ipsum, eu varius leo nisi id diam. Fusce sodales dolor lectus, vitae pellentesque risus hendrerit ac. Praesent feugiat at orci non gravida. Duis posuere porttitor tellus fringilla suscipit. Sed placerat dolor a ipsum scelerisque euismod. Donec hendrerit quam posuere convallis interdum. Nullam sem justo, ultricies sit amet imperdiet ac, commodo eget leo. Vestibulum sit amet interdum lectus. Donec rhoncus urna elit, semper varius orci imperdiet quis. Praesent tempus, nisl dictum viverra tempus, turpis libero volutpat nisi, at rutrum dolor nunc vitae ligula. Donec lobortis enim quam, at auctor velit consectetur in. Vestibulum mattis est at augue euismod, in congue neque dapibus. Proin vehicula tellus vel faucibus venenatis.','Created',0,10,0),(3,'titre 1','description 1','Created',0,42,0),(4,'titre 2','description 2','Created',0,10,0),(5,'titre 3','description 3','Created',0,100,0),(6,'titre 4','description 4','Created',0,20,0),(7,'Hellsing Ultimate','Lorem Venatum, consectetur adipiscing elit. Quisque ultrices, massa at venenatis gravida, nibh ante dictum ipsum !','Funding',42000,100000,0),(8,'Test projet','Test projet','Funding',420,1000,0);
/*!40000 ALTER TABLE `users_projet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_utilisateur`
--

DROP TABLE IF EXISTS `users_utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_utilisateur` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `karma` int unsigned NOT NULL,
  `porteur` tinyint(1) NOT NULL,
  `evaluateur` tinyint(1) NOT NULL,
  `financeur` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_utilisateur`
--

LOCK TABLES `users_utilisateur` WRITE;
/*!40000 ALTER TABLE `users_utilisateur` DISABLE KEYS */;
INSERT INTO `users_utilisateur` VALUES (1,'pbkdf2_sha256$150000$CvEcm1JCaHcY$koo61X7mO7LV8ePj0ci3mddNYEKC9NvwAuSH+SsusHQ=','2021-01-14 15:07:00.989355',0,'thomas','first','','t.b@email.com',1,1,'2021-01-04 17:26:07.600630',0,1,1,1),(2,'pbkdf2_sha256$150000$fkRAhjkkpuG6$BYcgddm/q7b56DeJzE074vYW++XlnCUFHqb1pL4vWeA=','2021-01-14 15:03:27.751029',1,'admin','','','',1,1,'2021-01-05 13:42:47.839572',0,0,0,0),(3,'pbkdf2_sha256$150000$W7gNPnPSssPn$4wlLkoA0I+txngd5qYXmW0fMQrOdoUj/lJLUHv287Ag=','2021-01-10 17:23:48.742604',0,'elia','eliz','mor','em@g.fr',0,1,'2021-01-10 17:15:08.288876',1,1,0,1),(6,'pbkdf2_sha256$150000$9W1uTMrEQM8z$ftPVFWosexP/hO/LaqHLBybtP3oC//huq1RJeMLB3pE=','2021-01-15 13:13:34.230892',0,'default','default','default','default@default.fr',0,1,'2021-01-15 13:13:33.977596',0,1,0,1),(7,'pbkdf2_sha256$150000$jS3foFMrUYoe$5qU6RnfLibcckv49nWdttiq+y3TJWHB0J/FsMNuNW6w=','2021-01-15 13:15:29.751128',0,'evaluateur','evaluateur','evaluateur','evaluateur@evaluateur.fr',0,1,'2021-01-15 13:15:29.511387',42,1,1,1);
/*!40000 ALTER TABLE `users_utilisateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_utilisateur_groups`
--

DROP TABLE IF EXISTS `users_utilisateur_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_utilisateur_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `utilisateur_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_utilisateur_groups_utilisateur_id_group_id_753c4b93_uniq` (`utilisateur_id`,`group_id`),
  KEY `users_utilisateur_groups_group_id_a71698e9_fk_auth_group_id` (`group_id`),
  CONSTRAINT `users_utilisateur_gr_utilisateur_id_a9ec7e2b_fk_users_uti` FOREIGN KEY (`utilisateur_id`) REFERENCES `users_utilisateur` (`id`),
  CONSTRAINT `users_utilisateur_groups_group_id_a71698e9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_utilisateur_groups`
--

LOCK TABLES `users_utilisateur_groups` WRITE;
/*!40000 ALTER TABLE `users_utilisateur_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_utilisateur_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_utilisateur_user_permissions`
--

DROP TABLE IF EXISTS `users_utilisateur_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_utilisateur_user_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `utilisateur_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_utilisateur_user_p_utilisateur_id_permissio_76fca242_uniq` (`utilisateur_id`,`permission_id`),
  KEY `users_utilisateur_us_permission_id_c32f2ac3_fk_auth_perm` (`permission_id`),
  CONSTRAINT `users_utilisateur_us_permission_id_c32f2ac3_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `users_utilisateur_us_utilisateur_id_fce4c46b_fk_users_uti` FOREIGN KEY (`utilisateur_id`) REFERENCES `users_utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_utilisateur_user_permissions`
--

LOCK TABLES `users_utilisateur_user_permissions` WRITE;
/*!40000 ALTER TABLE `users_utilisateur_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_utilisateur_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-21 15:06:51
