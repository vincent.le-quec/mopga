# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from enum import Enum

from django.contrib.auth.models import AbstractUser
from django.db import models
# Create your models here.
from django.utils import timezone


class Utilisateur(AbstractUser):
    karma = models.PositiveIntegerField(default=0)
    porteur = models.BooleanField(default=True)
    evaluateur = models.BooleanField(default=False)
    financeur = models.BooleanField(default=True)

    class Meta:
        permissions = (
            ('porteur', "une personne qui peut porter un projet"),
            ('evaluateur', "une personne qui peut évaluer le karma d'un projet"),
            ("financeur", "une personne qui peut financer un projet"),
        )


class Etat(Enum):
    created = "Created"
    funding = "Funding"
    closed = "Closed"
    rejected = "Rejected"


class Projet(models.Model):
    titre = models.CharField(max_length=256)
    description = models.CharField(max_length=1024)
    montant = models.FloatField(default=0)
    montantMax = models.FloatField(default=10)
    etat = models.CharField(max_length=10, default="Created", choices=[(tag, tag.value) for tag in Etat])
    karma = models.PositiveIntegerField(default=0)


class TransactionState(Enum):
    created = "Created"
    accepted = "Accepted"
    refused = "Refused"


class Financement(models.Model):
    date = models.DateTimeField(default=timezone.now)
    montant = models.FloatField(default=10)
    karma_claimed = models.BooleanField(default=False)
    state = models.CharField(max_length=20, default=TransactionState.created,
                             choices=[(tag, tag.value) for tag in TransactionState])
    transaction_ref = models.CharField(max_length=50, default="")
    idUser = models.ForeignKey("Utilisateur", on_delete=models.CASCADE)
    idProject = models.ForeignKey("Projet", on_delete=models.CASCADE)


class Creation(models.Model):
    date = models.DateTimeField(default=timezone.now)
    karma_claimed = models.BooleanField(default=False)
    idUser = models.ForeignKey("Utilisateur", on_delete=models.CASCADE)
    idProject = models.ForeignKey("Projet", on_delete=models.CASCADE)


class Evaluation(models.Model):
    note = models.PositiveIntegerField()
    idUser = models.ForeignKey("Utilisateur", on_delete=models.CASCADE)
    idProject = models.ForeignKey("Projet", on_delete=models.CASCADE)
