from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.me),  # my data
    path('<int:iduser>', views.iduser),
    re_path(r'^register$', views.register, name='register'),
    re_path(r'^login$', views.login_user, name='login'),
    re_path(r'^logout$', views.logout_user, name='logout'),
    re_path(r'^forbidden$', views.forbidden, name='forbidden'),
    re_path(r'^me$', views.me, name='me'),
    re_path(r'^legal$', views.legal, name='legal'),
    path('contact', views.contact, name='contact'),
]