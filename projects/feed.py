from django.contrib.syndication.views import Feed

from users.models import Projet


class ClovEarthProjectsFeed(Feed):
    title = "ClovEarth projects"
    link = "/projects/"
    description = "Updates on new projects on CloveEarth."

    def items(self):
        return Projet.objects.all()

    def item_title(self, item):
        return item.titre

    def item_description(self, item):
        return item.description

    def item_montant(self, item):
        return item.montant

    def item_montant_max(self, item):
        return item.montant_max

    def item_link(self, item):
        return '/projects/' + str(item.id)
