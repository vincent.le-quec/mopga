from django.urls import path

from projects import views
from projects.feed import ClovEarthProjectsFeed

urlpatterns = [
    path('', views.index),
    path('<int:idproject>', views.idproject),
    path('new', views.new),
    path('evaluate', views.evaluate),
    path('donate', views.donate),
    path('payment', views.payment),

    path('rss', ClovEarthProjectsFeed()),
    path('upload', views.uploadImage),

    path('logo', views.logo),
]