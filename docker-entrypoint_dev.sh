#!/bin/bash

echo "#################"
echo "## Entry point ##"
echo "#################"

## Update database
#echo "Update database"
#mysql -u root -p'root' -h database mopga < import.sql

# Apply database migrations
echo "Apply database migrations"
python manage.py makemigrations
python manage.py migrate

# Start server
echo "Starting server"
python manage.py runserver 0.0.0.0:8000