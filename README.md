# MOPGA

## Requirments

### Docker

```bash
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

```

### Docker compose

```bash
sudo apt-get remove docker-compose
sudo rm /usr/local/bin/docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo docker-compose --version
```

## Run

[localhost:8000](http://localhost:8000/)

### Development

1. `make build` Create image & run
2. `make run` Run project
3. `make dump` Create a dump of MySQL database
4. `make clean-dev` Stop a remove all containers of this project

### Production

1. `make prod` Create image & run production
2. `make clean-prod` Stop a remove all containers of this project

## Datamodel

![mcd](./mcd.png)

## Users

### Default user
porteur & financeur

- login: default
- password: password

### Evaluateur
porteur & financeur & evaluateur

- login: evaluateur
- password: password

### Staff
porteur & financeur & evaluateur & staff

- login: thomas
- password: password

### Admin
porteur & financeur & evaluateur & staff & admin

- login: admin
- password: admin