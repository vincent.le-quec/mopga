FROM python:3

ARG APP_USER=mopga
RUN groupadd -r ${APP_USER} && useradd --no-log-init -r -g ${APP_USER} ${APP_USER}

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir uwsgi

COPY . .

RUN python manage.py collectstatic --noinput

EXPOSE 8000

RUN chown -R ${APP_USER}:${APP_USER} /usr/src/app

USER ${APP_USER}:${APP_USER}

ENV UWSGI_WSGI_FILE=mopga/wsgi.py   \
    UWSGI_HTTP=:8000                \
    UWSGI_MASTER=1                  \
    UWSGI_HTTP_AUTO_CHUNKED=1       \
    UWSGI_HTTP_KEEPALIVE=1          \
    UWSGI_LAZY_APPS=1               \
    UWSGI_WSGI_ENV_BEHAVIOR=holy    \
    UWSGI_WORKERS=4                 \
    UWSGI_THREADS=2                 \
    UWSGI_STATIC_MAP="/static/=/usr/src/app/static/" \
    UWSGI_STATIC_EXPIRES_URI="/static/.*\.[a-f0-9]{12,}\.(css|js|png|jpg|jpeg|gif|ico|woff|ttf|otf|svg|scss|map|txt) 315360000"

CMD ["uwsgi", "--show-config"]
